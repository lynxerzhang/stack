package common.tool
{
/**
 * 将stack.js移植为as3版本
 * @see https://github.com/senocular/stack.js
 */
public class Stack 
{
	public function Stack() 
	{
	}
	
	private var stackMethodAry:Array = [];
	private var deferMethodAry:Array = [];
	private var isExec:Boolean = false;
	private var breakExecute:Boolean = false;
	private var lastExecuteReturnValue:*;
	
	/**
	 * 封装指定调用方法
	 * @param	s
	 * @param	m
	 * @param	...args
	 * @return
	 */
	public function invoked(s:*, m:* = null, ...args):Function {
		return function(...internalArgs):void {
			var method:StackMethod = new StackMethod(s, m, args);
			method.prependArgs.apply(null, internalArgs);
			stackMethodAry.push(method);
			exec();
		}
	}
	
	/**
	 * 执行各方法
	 * @param	s
	 * @param	m
	 * @param	...args
	 * @return
	 */
	public function exec(s:* = null, m:* = null, ...args):Boolean {
		this.push.apply(this, [s, m].concat(args));
		if (this.isExec) {
			return false;
		}
		this.isExec = true;
		deferMethodAry.length = 0;
		var stackMethod:StackMethod;
		while (this.stackMethodAry.length > 0 && !this.breakExecute) {
			stackMethod = this.stackMethodAry.shift();
			try {
				lastExecuteReturnValue = stackMethod.call();
			}
			catch (e:Error) {
				lastExecuteReturnValue = null;
				var errorStr:String = e.getStackTrace();
				if (errorStr) {
					trace(errorStr);
				}
				else {
					trace(e.message);
				}
			}
			if (!breakExecute && deferMethodAry.length > 0) {
				stackMethodAry.unshift.apply(stackMethodAry, deferMethodAry);
				deferMethodAry.length = 0;
			}
		}
		this.isExec = false;
		if (this.breakExecute) {
			this.clear();
		}
		return true;
	}
	
	/**
	 * 添加调用
	 * @param	s
	 * @param	m
	 * @param	...args
	 * @return
	 */
	public function push(s:*, m:* = null, ...args):Boolean {
		return pushItem(new StackMethod(s, m, args));
	}
	
	/**
	 * 添加调用
	 * @param	s
	 * @param	m
	 * @param	...args
	 * @return
	 */
	public function pushOnce(s:*, m:* = null, ...args):Boolean {
		var m:StackMethod = new StackMethod(s, m, args);
		remove(m, stackMethodAry, true);
		return pushItem(m);
	}
	
	private function pushItem(item:StackMethod):Boolean {
		if (item && item.isCallable()) {
			stackMethodAry.push(item);
			return true;
		}
		return false;
	}
	
	/**
	 * 延迟调用
	 * @param	s
	 * @param	m
	 * @param	...args
	 * @return
	 */
	public function defer(s:*, m:* = null, ...args):Boolean {
		return deferItem(new StackMethod(s, m, args));
	}
	
	/**
	 * 延迟调用
	 * @param	s
	 * @param	m
	 * @param	...args
	 * @return
	 */
	public function deferOnce(s:*, m:* = null, ...args):Boolean {
		var m:StackMethod = new StackMethod(s, m, args);
		remove(m, deferMethodAry, true);
		return deferItem(m);
	}
	
	private function deferItem(item:StackMethod):Boolean {
		if (item && item.isCallable()) {
			if (!isExec) {
				return false;
			}
			this.deferMethodAry.push(item);
			return true;
		}
		return false;
	}
	
	/**
	 * 清除各数组记录
	 */
	public function clear():void {
		this.stackMethodAry.length = 0;
		this.deferMethodAry.length = 0;
		this.isExec = false;
		this.lastExecuteReturnValue = null;
		this.breakExecute = false;
	}
	
	/**
	 * 退出当前执行循环
	 */
	public function kill():void {
		breakExecute = true;
	}
	
	/**
	 * 获取最后执行的方法的返回值
	 * @return
	 */
	public function value():*{
		return lastExecuteReturnValue;
	}
	
	private var removeIndex:Array = [];
	private function remove(item:StackMethod, list:Array, removeAll:Boolean = false):Boolean {
		var fun:Function = item.getFun();
		var index:int = list.length;
		var m:StackMethod;
		while (index--) {
			m = list[index];
			if (fun == m.getFun()) {
				removeIndex.push(index);
				break;
			}
		}
		if (removeIndex.length > 0) {
			while (removeIndex.length > 0) {
				list.splice(int(removeIndex.pop()), 1);
			}
			return true;
		}
		removeIndex.length = 0;
		return false;
	}
}
}

internal class StackMethod 
{
	private var stackMethodScope:*;
	private var stackMethod:Function;
	private var stackMethodAry:Array;
	private var methodReturnValue:*;
	
	public function StackMethod(s:*, m:* = null, ...args) 
	{
		var fun:Function;
		if (typeof s == "function") {
			fun = s;
			if (!(m is Array)) {
				args = m != null ? [m].concat(args) : [];
			}
			else {
				args = m;
			}
		}
		else {
			if (s != null) {
				if (m is String) {
					//该方法作用域需要为public
					fun = s[m];
				}
			}
			if (fun == null) {
				fun = m;
			}
			if (args && args.length > 0) {
				if (args[0] is Array) {
					args = args[0];
				}
			}
		}
		if (!(s is Function)) {
			stackMethodScope = s;
		}
		stackMethod = fun;
		stackMethodAry = args ? args : [];
		methodReturnValue = null;
	}
	
	/**
	 * 获取方法定义
	 * @return
	 */
	public function getFun():Function {
		return stackMethod;
	}
	
	/**
	 * 添加可选参数
	 * @param	...args
	 */
	public function prependArgs(...args):void {
		stackMethodAry.unshift.apply(stackMethodAry, args);
	}
	
	/**
	 * 判断方法是否可以调用
	 * @return
	 */
	public function isCallable():Boolean {
		return stackMethod != null;
	}
	
	/**
	 * 调用方法
	 * @param	...args
	 * @return
	 */
	public function call(...args):*{
		var fun:Function = getFun();
		if (fun == null) {
			methodReturnValue = null;
			return methodReturnValue;
		}
		prependArgs.apply(null, args);
		methodReturnValue = stackMethod.apply(stackMethodScope, stackMethodAry);
		return methodReturnValue;
	}
}
